# Häufig gestellte Frage (FAQ)

[In unserem README](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main/README.md) finden Sie neben Kontaktmöglichkeiten die FAQ auch auf Englisch.

### Auf meinem Hauptbildschirm werde ich gewarnt, dass mein Gerät nicht vollständig kompatibel ist. Was bedeutet das?

Wenn Sie diese Warnmeldung sehen, ist Ihr Gerät nur in der Lage, IDs von anderen Geräten zu empfangen, kann aber selbst keine aussenden. Sollten Sie sich entscheiden, andere im Fall einer Infektion zu benachrichtigen, wird diese Benachrichtigung leider niemanden erreichen, der nicht gleichzeitig mit Ihnen an einem Ort oder einer Veranstaltung eingecheckt war.

Dies wird durch eine Einschränkung in der Kombination aus verwendeter Hard- und Software verursacht. Dadurch kann Bluetooth LE peripheral mode nicht von CCTG verwendet werden. In manchen Fällen hilft dabei ein anderes Betriebssystem oder eine neuere Version dessen, wenn diese einen besseren Bluetooth-Treiber mitbringt. In anderen Fällen ist uns nichts bekannt, was Sie tun können, um das Problem zu lösen.

Die CWA [hat unsere Warnmeldung übernommen](https://github.com/corona-warn-app/cwa-app-android/pull/2481) und zeigt sie ab Version 2.2.

Wenn Sie diese Meldung auf dem Fairphone 2 sehen, aktualisieren Sie bitte Ihr Betriebssystem – auch dann, wenn Sie LineageOS verwenden.

Die Funktionalität zum Einchecken ist hiervor *nicht* betroffen.

### Mein Begegnungen-Tab zeigt keine IDs an, obwohl die Risiko-Ermittlung aktiviert ist. Was kann ich tun?

Normalerweise bedeutet das, dass der Scanner-Service im Hintergrund beendet wird.

* Aktivieren Sie in den App-Einstellungen die Funktion **Priorisierte Hintergrundaktivität**. Dies ist wichtig.
* Deaktivieren Sie für CCTG jegliche weiteren Batterispar-Dienste, die Ihr Gerätehersteller Ihnen anbietet. Hier finden Sie einige Hinweise und Anweisungen dazu: https://dontkillmyapp.com/?app=Corona%20Tracing
* Verzichten Sie wenn möglich darauf den Energiesparmodus Ihres Smartphones zu aktivieren, da dieser einen negativen Einfluss auf die Hintergrundaktualisierung der App haben kann.

Der Scanner-Service sollte immer dann neu starten, wenn Sie die Risiko-Ermittlung deaktivieren und wieder aktivieren.

Sollten Sie bemerken, dass der Scanner-Service erneut beendet wird, und Sie uns diesbezüglich kontaktieren möchten, dann informieren Sie uns bitte immer auch über Ihr Betriebssystem sowie Gerätehersteller und weisen Sie uns darauf hin, dass sie die obigen Schritte bereits befolgt haben.

### Sind QR-Codes für Veranstaltungen und Orte mit anderen Apps kompatibel?

Von der CWA wurde ein [Standard](https://github.com/corona-warn-app/cwa-documentation/blob/master/event_registration.md#qr-code-structure) festgelegt, nach dem Apps ihre QR-Codes generieren können, sodass sie kompatibel sind. Es gibt andere Apps, die angekündigt haben, diesen Standard adaptieren zu wollen, bislang sind noch keine Apps bekannt, die das umgesetzt haben.

### Kann ich CCTG auch außerhalb Deutschlands benutzen? Was, wenn ich nicht in Deutschland wohne?

Ja! Sie können über Begegnungen in jedem der Länder informiert werden, die in folgender Liste enthalten sind: https://www.coronawarn.app/de/faq/#interoperability_countries

Sie können keine Testergebnisse aus diesen Ländern abrufen. Sollten Sie jedoch infiziert sein, und wird Ihnen dies durch einen positiven PCR-Test bestätigt, dann können Sie [die TAN-Hotline anrufen](https://www.coronawarn.app/de/faq/#test_in_other_country), um einen Code zu erhalten, mit dem Sie andere warnen können.

Die Check-In-Funktionalität ist momentan nicht derartig gestaltet, dass sie international verwendet werden kann, denn Check-Ins werden nicht mit etwaigen Apps anderer Länder geteilt. Die Funktion kann aber theoretisch uneingeschränkt genutzt werden.

### Was genau unterscheidet CCTG von CWA?

Unabhänging davon, ob microG oder Google Play Services installiert sind, verwendet die offizielle Corona-Warn-App proprietäre Software, um mit der Exposure Notification API zu interagieren.

Corona Contact Tracing Germany (CCTG) ersetzt diesen proprietären Teil mit einer anderen Bibliothek vom [microG-Projekt](https://microg.org). Das bedeutet, dass CCTG (im Gegensatz zu CWA) in Gänze freie Software ist.

Darüberhinaus enthält unsere App alle benötigten Komponenten, um auch eigenständig zu funktionieren, wenn microG *nicht* auf Ihrem Smartphone installiert ist.

Des Weiteren hat CCTG folgende "exklusive" Funktionen, wobei wir aber jederzeit gewillt sind, unsere Verbesserungen zur CWA beizutragen:

* Inkompatibilitätswarnung ([cwa-app-android/#2481](https://github.com/corona-warn-app/cwa-app-android/pull/2481)) – ab CWA 2.2 enthalten
* Warnung über Batterieoptimierungen ([cwa-app-android/#2682](https://github.com/corona-warn-app/cwa-app-android/issues/2682))
* Unterstützung für Android 5 ([cwa-app-android/#1799](https://github.com/corona-warn-app/cwa-app-android/issues/1799#issuecomment-817148421), [cwa-app-android/#2026](https://github.com/corona-warn-app/cwa-app-android/pull/2026), [cwa-app-android/#2700](https://github.com/corona-warn-app/cwa-app-android/pull/2700), [cwa-app-android/#2844](https://github.com/corona-warn-app/cwa-app-android/pull/2844), [cwa-app-android/#2955](https://github.com/corona-warn-app/cwa-app-android/pull/2955))
* Begegnungen-Tab in der Menüleiste
* Durchsichtge Statusleiste ([cwa-app-android/#2483](https://github.com/corona-warn-app/cwa-app-android/issues/2483))
* Aufhebung der Rotationssperre
* Aufhebung der Backupsperre

Das Projekt muss auch Änderungen im Branding der App (Titel, Logo, Datenschutzrichtlinien, AGB, Impressum...) auf einem Stand mit der originalen Version zu halten.

### Warum zeigt die CWA eine Risikobegegnung an, CCTG aber nicht (oder umgekehrt)?

Beide Apps scannen periodisch im Hintergrund nach Begegnungen, aber nicht genau zur gleichen Zeit. Deswegen können beide Apps aufgrund von leicht unterschiedlichen Daten, die sie gesammelt haben, zu verschiedenen Schlüssen über Ihren Risikostatus gelangen.

### Warum zeigen mir microG/CWA Companion Risikobegegnungen an, die CCTG-App aber nicht?

Ab Version 1.9.1 benutzt die App Version 2 des ENF, mit der Änderungen in der Bewertung bzw. Erfassung von Risikobegegnungen eingeführt wurden (s. der [offizielle Blogpost](https://www.coronawarn.app/de/blog/2020-12-17-risk-calculation-exposure-notification-framework-2-0/)):

> Vereinfacht ausgedrückt: Unter dem Exposure Notification Framework in Version 2 werden vom Betriebssystem Begegnungen erfasst, die ein geringeres Risiko als „niedriges Risiko“ (grün) aufweisen. Diese sind aus aktueller epidemiologischer Sicht nicht relevant und werden von der Corona-Warn-App (CWA) herausgefiltert.

### Warum kann die App im Hintergrund laufen? Ich habe gehört, dass das eigentlich gar nicht geht.

Unter iOS mag das stimmen; auf Android ist von der Plattform vorgesehen, dass Sie Apps Ausnahmen von Batteriesparoptionen genehmigen können.

### Brauche ich microG oder Signature Spoofing um die App zu benutzen?

Nein, microG ist direkt in die App integriert, dadurch funktioniert sie auch auf Smartphones, auf denen microG nicht installiert ist. Wenn Sie microG auf Ihrem Smartphone bereits installiert haben, benutzt die App automatisch diese systemweite Installation anstatt ihrer eigenen integrierten.

### Warum benötigt die App Zugriff auf meinen Standort?

Die App greift weder auf Ihren GPS- noch Netzwerk-Standort zu.  
Für Android ist Bluetooth-Scanning allerdings eine Art von Standortzugriff, weil es theoretisch möglich ist, aus den gewonnenen Informationen Ihren ungefähren Standort zu ermitteln (s. https://stackoverflow.com/a/44291991/1634837).  
CCTG selbst versucht in keiner Form Ihren Standort zu bestimmen oder gar zu verfolgen.

In Android 11 erlaubt Google seiner Play-Services-Implementation des ENF Bluetooth-Scanning im Hintergrund auszuführen, [ohne spezielle Erlaubnis für die Standortermittlung](https://android.googlesource.com/platform/packages/apps/Bluetooth/+/refs/tags/android-11.0.0_r16/res/values/config.xml#118) beim Benutzer anzufragen. CCTG ist das natürlich nicht erlaubt, weswegen die App auch in Android 11 weiterhin Ihre Erlaubnis für die Standortermittlung benötigt.

Unter Android 11 ist es notwendig, den Standortzugriff durch die Einstellungen explizit auf *Immer erlauben* zu setzen. [Dieses Video](https://f2.tchncs.de/media_attachments/files/105/407/928/545/453/739/original/f4c78994b947af67.mp4) zeigt, wie das geht.

### Wie oft veröffentlicht ihr neue Updates? Wann werden sie bei F-Droid bereit gestellt?

Kurze Zeit nachdem ein Update von der Corona-Warn-App veröffentlich wird, veröffentlichen wir eine neue Version von CCTG. Diese neue Version ist sofort in unserem Repository verfügbar.

In der ersten Stufe unseres transparenten Staged Rollouts wird die App noch nicht als *vorgeschlagen* markiert. Dadurch empfiehlt Ihr F-Droid-Client sie Ihnen noch nicht als Update. Sie können sie natürlich trotzdem installieren, wenn Sie möchten.  
Wir tun dies, um von Benutzern, die sich aktiv dafür entscheiden eine möglicherweise noch nicht stabile Version der App auszuprobieren, Feedback über Abstürze und sonstige Probleme der App zu sammeln.

Sobald wir der Meinung sind, dass alle Probleme behoben wurden, markieren wir die neueste Version als *vorgeschlagen*, wodurch alle Benutzer unseres Repositorys das Update sofort erhalten, wenn ihr F-Droid-Client es das nächste Mal aktualisiert.

Einige Zeit später wird das Update dann auch im offiziellen F-Droid Repository verfügbar sein. Wie für F-Droid üblich kann dies eine Weile dauern, bitte seien Sie geduldig.  
Weil unsere App reproduzierbare Builds unterstützt, ist die über das offizielle F-Droid-Repository verfügbare APK exakt die gleiche wie die aus unserem Repository.

Folgen Sie unserem [Mastodon-Account](https://social.tchncs.de/@CCTG), um über neue Versionen der App informiert zu werden.

### Ich habe vorher CWA benutzt, möchte jetzt aber zu CCTG wechseln. Was muss ich tun?

Nachdem alle epidemologisch relevanten Daten automatisch nach zwei Wochen gelöscht werden, können Sie für zwei Wochen CCTG und CWA gleichzeitig benutzen und danach CWA deinstallieren. Deaktivieren Sie die Risikoermittlung in CWA, bevor Sie CCTG installieren, und benutzen Sie fortan CCTG, um sich bei Veranstaltungen und Orten einzuchecken.

#### Im Fall einer Infektion

Falls Sie positiv getestet werden und andere warnen möchten, rufen Sie die TAN-Hotline an und fragen Sie nach einer [zusätzlichen TAN](https://www.coronawarn.app/de/faq/#test_multiple_devices). Geben Sie an, dass Sie zwei separate Installationen besitzen, die unterschiedliche Daten gespeichert haben.

#### Interpretation von Risikowarnungen

Wenn Sie CWA mit microG verwenden, teilen sich CWA und CCTG die zufälligen Bluetooth-IDs und benutzen auch die von der jeweils anderen App gesammelten Daten als Grundlage für die Risikoermittlung. Die Anzeige, wie viele Tage eine App bereits installiert ist, spielt hierbei keine Rolle und ist für die Risikoermittlung nicht relevant.

Falls Sie CWA mit dem Exposure Notification System von Google benutzen, ist dies nicht der Fall. Dann kann jede App nur ihren eigenen Datensatz zur Risikoberechnung heranziehen.

In keinem dieser Fälle kann eine App die Veranstaltungen und Orte, in die sie mit der jeweils anderen App eingecheckt haben, für ihre Risikoberechnung verwenden. Jede App nutzt dafür nur die Daten, die sie in ihrem Check-in-Tab anzeigt.

### CCTG zeigt mir an, dass ich eine aktuellere Version von microG installieren soll, das funktioniert aber nicht. Was kann ich tun?

Einige Android-ROMs haben ihre eigene Version von microG integriert, die mit einem anderen Schlüssel als dem des microG-Projekts signiert ist.  
Falls das bei Ihnen der Fall sein sollte, bleibt Ihnen nichts anderes übrig als darauf zu warten, dass ihr ROM seine integrierte microG-Version aktualisiert, bevor Sie eine neuere Version von CCTG installieren und benutzen können.

Ab Version 1.9.1.X benötigt CCTG mindestens Version 0.2.15 von microG. Die Version davor (1.7.1) benötigt mindestens 0.2.14, was aber nicht überprüft bzw. erzwungen wurde.

Falls Sie CCTG bereits aktualisiert haben, können Sie versuchen wieder eine ältere Version der App zu installieren, die zu ihrer microG Version kompatibel ist (s. [dieser Kommentar](https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/51#issuecomment-165230)).  
Sie können CCTG auch einfach deinstallieren und die ältere Version neu installieren ohne Exposure-Daten zu verlieren, da diese in Ihrer microG Installation gespeichert sind. In diesem Fall werden allerdings ausstehende und bereits empfangene Testergebnisse gelöscht. Außerdem beginnt die App erneut von Tag 0 an zu zählen, was jedoch, wie oben bereits beschrieben, keinen weiteren Einfluss auf die Funktion der App hat.

### Wo finde ich die Exposure Notification-Einstellungen von microG in der CCTG-App?

Seit Version 1.13.2.0 gibt es einen eigenen Tab für den microG-Begegnungsgraphen in der Menüleiste. Wenn Sie jedoch die anderen Bildschirme der microG-Exposure Notification-Einstellungen aufrufen möchten, können Sie weiterhin diese Schritte befolgen.

Öffnen Sie die Einstellungen zur Risiko-Ermittlung, indem Sie auf den Abschnitt über Ihrem Risiko-Status klicken, der anzeigt, dass die Risiko-Ermittlung aktiv ist (bzw. Bluetooth/Standort deaktiviert ist). Tippen Sie nun auf "Erweiterte Einstellungen öffnen". Daraufhin werden Sie automatisch zu den Exposure Notification-Einstellungen von microG weitergeleitet. Dabei wird berücksichtigt, ob die App ein externes, systemweit installiertes oder das in die App integrierte microG verwendet. Falls Sie sich immer noch unsicher sind, was Sie tun müssen, könnte das Video Ihnen helfen, was [an diesen Post angehängt](https://social.tchncs.de/@CCTG/105508716416465555) ist.

Öffnen Sie alternativ **App-Informationen** im Overflow-Menü und klicken Sie auf die Statusmeldung am Ende der Seite. Dadurch werden die selben Einstellungen angezeigt. In früheren Versionen von CCTG war dies der einzige Weg.

### Offizielle FAQ der Corona-Warn-App

CWA hat ebenfalls eine FAQ, die teilweise auch auf CCTG zutrifft.

#### Corona-Warn-App-Project

* EN: https://www.coronawarn.app/en/faq/
* DE: https://www.coronawarn.app/de/faq/

#### Bundesregierung

* DE: https://www.bundesregierung.de/corona-warn-app-faq
* EN: https://www.bundesregierung.de/corona-warn-app-faq-englisch

