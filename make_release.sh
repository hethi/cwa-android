#!/bin/bash

set -e

cd fdroidrepo
fdroid build -s -l --server de.corona.tracing
fdroid publish
fdroid update
rm -r ../../cctg-pages/fdroid/repo
cp -r ./repo ../../cctg-pages/fdroid/
pushd ../../cctg-pages/
git add .
git commit -am "update version"
git push
popd
fdroid deploy
