# Corona Contact Tracing Germany

## Talk to us

* Join the matrix room: [#cctg:bubu1.eu](https://matrix.to/#/#cctg:bubu1.eu), also bridged to XMPP: xmpp:cctg@conference.jabber.de?join
* Follow us on Mastodon: [@CCTG@social.tchncs.de](https://social.tchncs.de/@CCTG)

## Get The App

Our [F-Droid repository](https://bubu1.eu/cctg/fdroid/repo/?fingerprint=f3f30b6d212d84aea604c3df00e9e4d4a39194a33bf6ec58db53af0ac4b41bec) contains *beta* versions and is updated with new releases immediately. After a few days, stable releases will also be available in the standard f-droid.org repo. You can also verify the builds yourself, see [Reproducible Builds](#reproducible-builds).

<p align="center">
<a href="https://f-droid.org/packages/de.corona.tracing">
<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    width="180px">
</a>
<br>
<a href="https://liberapay.com/CCTG/">
<img src="https://liberapay.com/assets/widgets/donate.svg" alt="Donate" width="105px">
</a>
</p>

## About

This is a fork of [CWA](https://github.com/corona-warn-app/cwa-app-android/) without proprietary dependencies. While the German Corona Warn App itself is Free Software, it depends on Google's proprietary [Exposure Notification Framework](https://www.google.com/covid19/exposurenotifications/). This fork instead uses the drop-in compatible [microg implementation](https://github.com/microg/GmsCore/issues/1166).

While we've had many reports of users successfully running the app on various Android versions it's by far not as well tested as the official Corona-Warn-App. Use at your own risk! It should work on any Android 5+ device regardless of installed play-services or microg versions.

If microG is already installed on your system, it will make use of its Exposure Notifications implementation. Otherwise, it will use the bundled implementation. It will never connect to the Google Play services Exposure Notification System. You can find out which implementation is used by checking the status at the bottom of the App Information screen.

## Translations

There's currently no translation system in place as most translation are taken directly from CWA. There's some small bits left to translate/fix for Romanian, Bulgarian and Turkish. Please contact us!

* Polish Translations of our text additions contributed by [GenomZ](https://github.com/GenomZ)
* Bulgarian contributed by [Fen](https://vulpine.club/@fennecbyte)

# FAQ

The FAQ is [also available in German](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main/FAQ_de.md).

### There's a card on my home screen telling me that my device is only partially compatible. What does this mean?

If you see such a notice, your device is capable of collecting IDs from other devices, but not able to send any itself via Bluetooth. In case you are infected and you decide to warn others, nobody (outside of events and locations you checked in to) will be able to receive your warning, unfortunately.

This is caused by a restriction in the combination of hardware and software that doesn't allow CCTG to make use of Bluetooth LE peripheral mode. In some cases, a different operating system or newer OS version may bring a Bluetooth driver that has support for this advertising mode, which can fix the issue. In other cases, we don't know of anything you can do to resolve the issue.

CWA [adopted this warning message from us](https://github.com/corona-warn-app/cwa-app-android/pull/2481) and ships it from version 2.2 onwards.

On Fairphone 2, please update your operating system, including if you are using LineageOS.

The Check-In functionality is *not* affected.

### My Encounters tab shows no IDs even though Exposure Logging is enabled. What can I do?

This usually means that the scanner service is getting stopped in the background.

* Activate "Prioritized Background Activity" in the app's settings menu. This is most important.
* Disable any other battery-saving services that your Android vendor is providing to you for CCTG. There is some information and instructions available on this website: https://dontkillmyapp.com/?app=Corona%20Tracing
* Avoid to enable your phone's battery saving mode when possible. It's likely that this will make the scanning functionality less reliable.

The scanner service should restart whenever you manually disable Exposure Logging and enable it again.

If you notice that scanning stops again eventually and you would like to contact us regarding this, it's best if you include details about your operating system and device vendor, as well as an indication that you followed the instructions above.

### Are QR codes for events and locations compatible with other apps?

CWA has worked out a [standard](https://github.com/corona-warn-app/cwa-documentation/blob/master/event_registration.md#qr-code-structure), according to which compatible QR codes can be generated. Other apps have announced an intention to adapt this standard. So far, no cases where this has been implemented are known to us.

### Can I use CCTG outside of Germany? What if I don't live there?

Yes! You can be warned about Bluetooth encounters in each of the countries that are included in this list: https://www.coronawarn.app/en/faq/#interoperability_countries

You can not receive test results from these countries. In case you are infected and confirmed this through a PCR test, however, you may [call the TAN hotline](https://www.coronawarn.app/en/faq/#test_in_other_country) to receive a code that allows you to warn others.

Check-In functionality is not designed to work internationally at the moment, as check-ins are not shared with contact tracing applications of other countries or those apps don't provide such functionality at all. However, theoretically, the functionality is available without limitations.

### What is the difference to CWA?

The official Corona-Warn-App build contains a proprietary component to interact with the Exposure Notifications API, even if microg is installed instead of Google Play Services.

Corona Contact Tracing Germany replaces this proprietary component with a different library provided by the [microg](https://microg.org) project, meaning that it is built as fully free software (in contrast to Corona-Warn-App).

Our app also ships with the relevant components to also function as a standalone app if microg is *not* installed.

Additionally, we have the following "exclusive" features, though we are always willing to contribute our improvements back to CWA:

* Incompatibility warning card ([cwa-app-android/#2481](https://github.com/corona-warn-app/cwa-app-android/pull/2481)) – included from CWA 2.2 onwards
* Battery optimizations warning card ([cwa-app-android/#2682](https://github.com/corona-warn-app/cwa-app-android/issues/2682))
* Android 5 support ([cwa-app-android/#1799](https://github.com/corona-warn-app/cwa-app-android/issues/1799#issuecomment-817148421), [cwa-app-android/#2026](https://github.com/corona-warn-app/cwa-app-android/pull/2026), [cwa-app-android/#2700](https://github.com/corona-warn-app/cwa-app-android/pull/2700), [cwa-app-android/#2844](https://github.com/corona-warn-app/cwa-app-android/pull/2844), [cwa-app-android/#2955](https://github.com/corona-warn-app/cwa-app-android/pull/2955))
* Encounters bottom tab
* Translucent status bar ([cwa-app-android/#2483](https://github.com/corona-warn-app/cwa-app-android/issues/2483))
* Lifted rotation lock
* Lifted backup block

The project also has to keep changes related to branding (app title, icon, privacy policy, terms of service, imprint…) in sync with new upstream versions.

Due to an upstream decision, CWA's data donation (privacy preserving analytics, PPA) and survey features are not available in CCTG, as they require Google SafteyNet attestation. ([cwa-wishlist/#356](https://github.com/corona-warn-app/cwa-wishlist/issues/356))

### CWA shows some exposures but CCTG doesn't, or the other way round.

Both apps are periodically scanning for encounters in the background, but not exactly at the same time. Therefore, both apps can collect slightly different data and reach different conclusions about your risk status.

### microG/CWA Companion shows some exposures but the app says there were none.

Starting with version 1.9.1 this is expected, see the [official blog post](https://www.coronawarn.app/en/blog/2020-12-17-risk-calculation-exposure-notification-framework-2-0/):

> In simplified terms: Under Exposure Notification Version 2.0, the operating system also logs encounters with a risk lower than "low risk" (green). However, since these encounters are not relevant from the current epidemiological perspective, the Corona-Warn-App filters them out.

### How does the app manage to stay alive in the background? Didn't everyone say that's impossible?

This may be correct for iOS; for Android, the platform is supposed to allow you to grant exceptions from battery optimization procedures.

### Do I need microG/signature spoofing for this?

No, this app bundles a standalone version of the microG implementation that will get used when there's no microG system installation found.

### Why does the app need location permission?

The app doesn't access GPS or Network location but Android considers bluetooth scanning a form of location access (because you could derive location information from the info you could get there), see here for details: https://stackoverflow.com/a/44291991/1634837. CCTG doesn't do any location tracking though.

On Android 11 Google allowed the play services ENF implementation to do bluetooth scanning in the background [without special location permission](https://android.googlesource.com/platform/packages/apps/Bluetooth/+/refs/tags/android-11.0.0_r16/res/values/config.xml#118). CCTG isn't whitelisted of course and thus needs to still ask for full location permission in Android 11.

On Android 11, it is necessary to explicitly set location access to Always enabled through the settings menu in order to enable Exposure logging. [This video](https://f2.tchncs.de/media_attachments/files/105/407/928/545/453/739/original/f4c78994b947af67.mp4) demonstrates how to do that.

### When will updates be released? When is the app coming to F-Droid?

Shortly after an update to CWA is released, we will release a new version of CCTG. New versions are immediately available on our repo.

In the first stage of our "transparent staged rollout", the app is not marked as "suggested" yet, meaning that your F-Droid client won't recommend it as an update, though you can install it manually. We do this to be informed about crashes and other issues by users who knowingly choose to try out a version that might not be stable yet.

Once we feel that all issues are sorted out, we will mark the most recent version as "suggested", causing users of our repository to receive the update immediately (once their F-Droid client refreshes the repository).

After some time, the latest version also appears in the official F-Droid repository, though as usual for F-Droid, this can take a while. Therefore, please be patient. F-Droid will serve exactly the APK that we also have in our repo, because our app builds reproducibly.

You might want to follow our [mastodon account](https://social.tchncs.de/@CCTG), where we will announce new versions.

### How to migrate from CWA

Since all epidemologically relevant data automatically gets deleted after two weeks, you can use both CCTG and CWA at the same time for two weeks and uninstall CWA afterwards. Disable Exposure Logging in CWA before installing CCTG and begin using CCTG to check in to events and locations.

#### In case of an infection

In case you are tested positive and would like to warn others, call the TAN hotline and ask for an [additional TAN](https://www.coronawarn.app/en/faq/#test_multiple_devices). State that you have been using two separate installations that store different data.

#### Interpreting risk warnings

If you are running CWA with microG, CWA and CCTG share the random Bluetooth IDs and will both use those that the other app had collected as a basis for their risk calculation. The indicatior that shows how many days ago the respective app was installed is purely cosmetic and not relevant for risk calculation.

If you are running CWA with Google's Exposure Notification System, this is not the case and each app is only able to use the dataset that it has collected itself.

In none of these situations is any of the apps able to use the events and locations of the other app for risk calculation. Each app only uses the items that are shown in their respective Check In tab.

### CCTG says I need to update microG but this isn't working

Some ROMs come with a version of microG that is signed with a different key from the one the microG project distributes, in this case you need to wait for your ROM to update their built-in microG version before you can use the new CCTG version.

CCTG version 1.9.1.X needs microG at least version 0.215 to work. The previous version (1.7.1) needed at least version 0.2.14 but this wasn't enforced.

If you already updated you can try following [this comment](https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/51#issuecomment-165230) to see how to downgrade to 1.7.1 again. You can also uninstall CCTG and reinstall the older version. Your exposure data (but not pending or received test results) will be kept intact inside the microG installation. The app will start counting from day 0 again but this makes no functional difference.

### How do I access the microG exposure notification settings in the standalone app?

Since version 1.13.2.0, there is a bottom tab for the encounters graph from microG. If you however want to access the other microg Exposure Notification settings screens, you can still follow these steps:

Open the exposure logging settings by clicking on the "Exposure logging active" (or "Exposure logging stopped", etc.) section above your risk status on the main screen. From there, select "Open advanced settings". It'll magically take to to the correct (integrated or external) microG EN settings page. If you're still unsure what to do, [the video attached to this post may help you](https://f2.tchncs.de/media_attachments/files/105/508/703/503/280/388/original/1993d9833ff25e92.mp4).

Alternatively, go to App Information (via the three-dot/kebab menu) and then tap on the status text at the very bottom. This will bring you to the same screen. This used to be the only option in earlier versions.


### Official Corona-Warn-App FAQ

The FAQ for the Corona-Warn-App partially also apply to CCTG.

#### Corona-Warn-App project

* EN: https://www.coronawarn.app/en/faq/
* DE: https://www.coronawarn.app/de/faq/

#### Federal government

* DE: https://www.bundesregierung.de/corona-warn-app-faq
* EN: https://www.bundesregierung.de/corona-warn-app-faq-englisch


## Reproducible Builds

See [docs/rebuilding.md](./docs/rebuilding.md) on how to reproduce the official builds.

