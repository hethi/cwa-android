package de.rki.coronawarnapp.ui.launcher

import android.app.Application
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import de.rki.coronawarnapp.environment.BuildConfigWrap
import de.rki.coronawarnapp.main.CWASettings
import de.rki.coronawarnapp.storage.OnboardingSettings
import de.rki.coronawarnapp.update.MicroGChecker
import de.rki.coronawarnapp.update.UpdateChecker
import de.rki.coronawarnapp.util.coroutine.DispatcherProvider
import de.rki.coronawarnapp.util.ui.SingleLiveEvent
import de.rki.coronawarnapp.util.viewmodel.CWAViewModel
import de.rki.coronawarnapp.util.viewmodel.SimpleCWAViewModelFactory

class LauncherActivityViewModel @AssistedInject constructor(
    private val updateChecker: UpdateChecker,
    dispatcherProvider: DispatcherProvider,
    val application: Application,
    private val cwaSettings: CWASettings,
    private val onboardingSettings: OnboardingSettings
) : CWAViewModel(dispatcherProvider = dispatcherProvider) {

    val events = SingleLiveEvent<LauncherEvent>()
    private val microGChecker = MicroGChecker(application.applicationContext)

    init {
        launch {
            val updateResult = updateChecker.checkForUpdate()
            val microGResult = microGChecker.checkForUpdate()
            when {
                updateResult.isUpdateNeeded -> LauncherEvent.ShowUpdateDialog(updateResult.updateIntent?.invoke()!!)
                microGResult.isUpdateNeeded -> LauncherEvent.ShowMicroGUpdateDialog(microGResult.updateIntent?.invoke()!!)
                isJustInstalledOrUpdated() -> LauncherEvent.GoToOnboarding
                else -> LauncherEvent.GoToMainActivity
            }.let { events.postValue(it) }
        }
    }

    private fun isJustInstalledOrUpdated() =
        !onboardingSettings.isOnboarded || !cwaSettings.wasInteroperabilityShownAtLeastOnce ||
            cwaSettings.lastChangelogVersion.value < BuildConfigWrap.VERSION_CODE

    @AssistedFactory
    interface Factory : SimpleCWAViewModelFactory<LauncherActivityViewModel>
}
